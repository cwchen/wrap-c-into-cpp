/* point.h */
#pragma once

/* An opaque struct. */
typedef struct point_t point_t;

#ifdef __cplusplus
extern "C" {
#endif

    point_t * point_zero(void);
    point_t * point_new(double x, double y);
    void point_delete(void *self);
    double point_x(point_t *self);
    void point_set_x(point_t *self, double x);
    double point_y(point_t *self);
    void point_set_y(point_t *self, double y);
    double point_distance_between(point_t *p, point_t *q);

#ifdef __cplusplus
}
#endif