/* cpppoint.hpp */
#pragma once

typedef struct point_private_t point_private_t;

class Point {
public:
    Point() : Point(0.0, 0.0) {};
    Point(double x, double y);
    ~Point();
    double x();
    void setX(double x);
    double y();
    void setY(double y);
    static double distanceBetween(Point *p, Point *q);
private:
    point_private_t *fields;
};