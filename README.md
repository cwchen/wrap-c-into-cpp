# Wrap C Code into C++ Classes

This tiny program demonstrates how to wrap C code into C++ classes.

## System Requirements

* C++ compiler (Clang or GCC)
* GNU Make (for compilation only)

## Usage

Clone the project:

```
$ git clone https://github.com/cwchentw/wrap-c-into-cpp.git
```

Move your working directory to the root of the repo:

```
$ cd wrap-c-into-cpp
```

Compile the application:

```
$ make LDFLAGS=-lm
```

Run the compiled program:

```
$ ./dist/program && echo $?
0
```

## Copyright

Copyright (c) 2021 Michelle Chen. Licensed under MIT.
