/* main.cpp */
#include <iostream>
#include <new>
#include "cpppoint.hpp"


int main(void)
{
    Point *p = NULL;
    Point *q = NULL;
    double d;

    try {
        p = new Point();
    }
    catch (std::bad_alloc &ba) {
        std::cerr << ba.what() << std::endl;
        goto ERROR_MAIN;
    }

    try {
        q = new Point(3.0, 4.0);
    }
    catch (std::bad_alloc &ba) {
        std::cerr << ba.what() << std::endl;
        goto ERROR_MAIN;
    }

    if (!(3.0 == q->x())) {
        std::cerr << "Wrong value x: " << q->x() << std::endl;
        goto ERROR_MAIN;
    }

    if (!(4.0 == q->y())) {
        std::cerr << "Wrong value y: " << q->y() << std::endl;
        goto ERROR_MAIN;
    }

    d = Point::distanceBetween(p, q);
    if (!(5.0 == d)) {
        std::cerr << "Wrong distance: " << d << std::endl;
        goto ERROR_MAIN;
    }

    q->setX(5.0);
    q->setY(12.0);

    d = Point::distanceBetween(p, q);
    if (!(13.0 == d)) {
        std::cerr << "Wrong distance: " << d << std::endl;
        goto ERROR_MAIN;
    }

    delete p;
    delete q;

    return 0;

ERROR_MAIN:
    if (q)
        delete q;

    if (p)
        delete p;

    return 1;
}