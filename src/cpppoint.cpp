/* cpppoint.cpp */
#include <cstdlib>
#include <new>
#include "point.h"
#include "cpppoint.hpp"

struct point_private_t {
    point_t *pt;  
};

Point::Point(double x, double y)
{
    fields = \
        (point_private_t *) malloc(sizeof(point_private_t));
    if (!fields)
        throw new std::bad_alloc();

    fields->pt = point_new(x, y);
    if (!(fields->pt)) {
        free((void *) fields);
        fields = NULL;
        throw new std::bad_alloc();
    }
}

Point::~Point()
{
    point_delete((void *) (fields->pt));

    if (fields)
        free((void *) fields);
}

double Point::x()
{
    return point_x(fields->pt);
}

void Point::setX(double x)
{
    point_set_x(fields->pt, x);
}

double Point::y()
{
    return point_y(fields->pt);
}

void Point::setY(double y)
{
    point_set_y(fields->pt, y);
}

double Point::distanceBetween(Point *p, Point *q)
{
    return point_distance_between(p->fields->pt, q->fields->pt);
}